import React, { Component } from 'react';
import { makeStyles } from "@material-ui/core/styles";

// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import { Link } from "react-router-dom";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { Table } from 'react-bootstrap';
import axios from 'axios';
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import FilterList from "@material-ui/icons/FilterList";
import CardIcon from "components/Card/CardIcon.js";

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
import Warning from "@material-ui/icons/Warning";
import { Container } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
const defaultMeasuremodel = {
  name: '',
  dimension: [],
  metric: [],
  operator: [],
  type: [],

}
const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};
const useStyles = makeStyles(styles);
const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: [],

}
const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {

        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}


export default class EditFilter extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      measureMetaModelUrl: 'http://localhost:55183/measure',
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      loading: false,
      relaxMetaModelModal: false,
      measureModel: defaultMeasuremodel,
      dimension: [],
      name_dim: [],
      name_level: [],
      name_metric: [],
      name_attribute: [],
      filters: [],
      name: '',
      description: '',
      extension_point: '',
      kind: '',
      att_name: "",
      att_dim_dimension: "",
      att_dim_level: "",
      editfilters: [],
      allFilters: [],
      attribute: [],
      dim: [],
      edit: [],
      metric: "",
      function: '',
      selectedOption: '',
      metricError: "",
      nameError: "",
      name_attr: "",
      dimError: "",
      levelError: ""



    };

    this.handleInputChange = this.handleInputChange.bind(this);

  }
  fetchMeasure() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.measureMetaModelUrl)])
      .then(([measureModelResponse]) => {
        const measureModel = measureModelResponse.model;
        this.setState({
          measureModel, loading: false,
          dimension: measureModel.dimension,
          name_dim: measureModel.dimension.map(dim => dim.name),
          name_level: measureModel.dimension.map(dim => dim.level.map(name => name.name)),

          name_metric: measureModel.metric.map(metric => metric.name),
          name_attribute: measureModel.dimension.map(dim => dim.level.map(attr => attr.attribute.map(m => m.name))),


        });

        var mergedattribute = [].concat.apply([], this.state.name_attribute);
        var attr = [].concat.apply([], mergedattribute);
        var mergedlevel = [].concat.apply([], this.state.name_level);
        var level = [].concat.apply([], mergedlevel);
        this.setState({ name_attribute: attr, name_level: level });

      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })
  }



  fetchfilter() {

    this.setState({ loading: true });
    getMetaModelPromise(this.state.relaxMetaModelUrl)
      .then((response) => {

        const metaModel = response.model_response.relax_model;
        console.log('relax model', metaModel)

        this.setState({
          metaModel, loading: false,
          allFilters: metaModel.filter,
          attribute: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].attribute,
          filters: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername),
          name: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].name,
          description: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].description,
          extension_point: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].extension_point,
          kind: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].kind,
          att_name: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].attribute ? metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].attribute.name : undefined,
          att_dim_dimension: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].attribute ? metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].attribute.dim.dimension : undefined,
          att_dim_level: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].attribute ? metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].attribute.dim.level : undefined,
          metric: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].metric,
          function: metaModel.filter.filter(filter => filter.name == this.props.match.params.filtername)[0].function,
        });

        console.log("metric", this.state.metric);

      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }

  editFilter(newFilter) {
    console.log('hello from editFilter')
    const editfilters = this.state.allFilters.filter(filter => filter.name !== this.props.match.params.filtername);
    const edit = [...editfilters, newFilter];
    this.setState({ edit });
    console.log("erfr", this.state.edit);
    const apiUrl = 'http://localhost:55183/frontend/update-relax-model';
    const dataUrl = 'http://localhost:55183/update-relax-model';
    console.log("ddsd", editfilters);
    const headers = new Headers();
    const options = {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ ...this.state.metaModel, filter: edit })
    }
    Promise.all([fetch(apiUrl, options), fetch(dataUrl, options)])
      .then(
        (result) => {
          console.log('result:', result)
          this.setState({
            response: result,
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }
  validate() {
    let nameError = "";
    let metricError = "";
    let dimError = "";
    let levelError = ""
    if (this.state.metric !== undefined) {
      if (!this.state.name_metric.find(v => v === this.state.metric)) { metricError = "metric is not valid"; }
    }
    if (this.state.attribute !== undefined) {

      if (!this.state.name_attribute.find(v => v === this.state.att_name)) { nameError = "name attribute is not valid"; }

    }
    if (this.state.attribute !== undefined) {

      if (!this.state.name_dim.find(v => v === this.state.att_dim_dimension)) { dimError = "dimension is not valid"; }

    }
    if (this.state.attribute !== undefined) {

      if (!this.state.name_level.find(v => v === this.state.att_dim_level)) { levelError = "level is not valid"; }

    }

    if (metricError || nameError || dimError || levelError) {
      this.setState({ metricError: metricError, nameError: nameError, dimError: dimError, levelError: levelError });
      return false;
    }




    return true;
  }

  newItem(kind) {
    switch (kind) {
      case 'FILTERED_ATTRIBUTE':
        return {
          name: this.refs.name.value,
          description: this.refs.description.value,
          extension_point: this.refs.extension_point.value,
          kind: this.refs.kind.value,
          attribute: {
            name: this.refs.att_name.value, dim: {
              level: this.refs.att_dim_level.value,
              dimension: this.refs.att_dim_dimension.value,
            }
          }

        };


      case 'FILTERED_METRIC':
        return {
          name: this.refs.name.value,
          description: this.refs.description.value,
          extension_point: this.refs.extension_point.value,
          kind: this.refs.kind.value,
          metric: this.refs.metric.value,

        };

      case 'UNFILTERED_METRIC':
        return {
          name: this.refs.name.value,
          description: this.refs.description.value,
          extension_point: this.refs.extension_point.value,
          kind: this.refs.kind.value,
          metric: this.refs.metric.value,

        };

      case 'FUNCTION':
        return {
          name: this.refs.name.value,
          description: this.refs.description.value,
          extension_point: this.refs.extension_point.value,
          kind: this.refs.kind.value,
          function: this.refs.function.value,

        };

      default:
        return kind;
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const newFilter = this.newItem(this.state.kind);
    console.log('newFilter', newFilter)


    //this.setState({metaModel:[...this.state.metaModel]})
    const isValid = this.validate();
    console.log('validation data', isValid)
    if (isValid) {
      this.editFilter(newFilter);
      this.setState({ metricError: "", nameError: "", levelError: "", dimError: "" });

    }


  }
  handleInputChange(e) {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }


  componentDidMount() {
    this.fetchfilter();
    this.fetchMeasure();


  }
  render() {
    const { filters } = this.state;
    let message;
    if (this.state.kind === 'FILTERED_ATTRIBUTE') {
      message = <div> <label> Attribute :
        <Container>
        <div> 
        <label htmlFor="att_name">Attribute Name :
          <input type="texta" name="att_name" ref="att_name" value={this.state.att_name} onChange={this.handleInputChange} />
          
          {this.state.nameError ?
            <div className= "my-notify-error">{this.state.nameError}</div> : null}
            </label>
      
        </div>
    <label>Dim :
      <Container>
    <label htmlFor="att_dim_dimension">Dimension :
          <input type="texta" name="att_dim_dimension" ref="att_dim_dimension" value={this.state.att_dim_dimension} onChange={this.handleInputChange} />
         
      
        {this.state.dimError ?
          <div className= "my-notify-error">{this.state.dimError}</div> : null}</label>
         <label htmlFor="att_dim_level">Level :
          <input type="texta" name="att_dim_level" ref="att_dim_level" value={this.state.att_dim_level} onChange={this.handleInputChange} />
        {this.state.levelError ?
          <div className= "my-notify-error">{this.state.levelError}</div> : null}</label></Container> </label></Container> </label></div>;
    }
    else if (this.state.kind === 'FILTERED_METRIC') {
      message = <div>
          <label htmlFor="metric">Metric :
        <input type="texta" name="metric" ref="metric" value={this.state.metric} onChange={this.handleInputChange} />
    

        {this.state.metricError ?
          <div className= "my-notify-error">{this.state.metricError}</div> : null}</label>
      
      </div>;
    }
    else if (this.state.kind === 'UNFILTERED_METRIC') {
      message = <div>
      <label htmlFor="metric">Metric :
        <input type="texta" name="metric" ref="metric" value={this.state.metric} onChange={this.handleInputChange} />
      
        {this.state.metricError ?
          <div className= "my-notify-error">{this.state.metricError}</div> : null}</label>

      </div>;
    } else if (this.state.kind === 'FUNCTION') {
      message = <div>
           <label htmlFor="function">FUNCTION :
        <input type="texta" name="function" ref="function" value={this.state.function} onChange={this.handleInputChange} />
        </label>
      </div>;
    }

    return (<div><GridContainer><GridItem xs={12} sm={12} md={4}>

<Card>
        <CardHeader color="info" stats icon>
                <CardIcon color="info">
                  <FilterList />
                </CardIcon>
                <p className={useStyles.cardCategory}>{this.props.match.params.filtername}</p>
              </CardHeader>



      
        <CardBody>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th >Description</th>
                <th >Extension point</th>

              </tr>
            </thead>
            <tbody>
              {filters.map(filter =>
                <tr key={filter.name}>
                  <td>{filter.description}</td>
                  <td> {filter.extension_point}
                  </td>

                </tr>
              )}
            </tbody>
          </Table>
          <Link className="btn btn-2 btn-sep icon-cart" to="/admin/table">Back</Link>
        </CardBody>
      </Card>
    </GridItem>
      <GridItem xs={12} sm={12} md={8} >
        <Card>
          <CardHeader color="success">
            <h4 className={useStyles.cardTitleWhite}>Edit {this.props.match.params.filtername}</h4>

          </CardHeader>
          <CardBody>
            <div>
              <br />

              <form onSubmit={this.onSubmit.bind(this)}>
             
                  <label htmlFor="name">Name :
              
                  <input disabled type="texta" name="name" ref="name" value={this.state.name} onChange={this.handleInputChange} />
                  </label>
                  <label htmlFor="description">Description :
                  <input type="texta" name="description" ref="description" value={this.state.description} onChange={this.handleInputChange} />
                  </label>
                  <label htmlFor="extension_point">Extension point :
                  <input type="texta" name="extension_point" ref="extension_point" value={this.state.extension_point} onChange={this.handleInputChange} />
                  </label>
               <div>
                  <label htmlFor="kind">Kind :
                  <select name="kind" ref="kind" value={this.state.kind} onChange={(e) => { this.setState({ kind: e.target.value }) }}>
                    <option value="FILTERED_ATTRIBUTE">FILTERED_ATTRIBUTE</option>
                    <option value="FILTERED_METRIC">FILTERED_METRIC</option>
                    <option value="UNFILTERED_METRIC">UNFILTERED_METRIC</option>
                    <option value="FUNCTION">FUNCTION</option>

                  </select>
              </label>
              </div>

                <div>{message}</div>



                <button type="submit" value="Save" className="btn btn-4 btn-sep icon-send" >Save</button>
              </form>
            </div>

          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
    </div>);
  }

}



