import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, ButtonToolbar } from 'react-bootstrap';
import { Link } from "react-router-dom";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { Table } from 'react-bootstrap';
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import 'core-js/es6/promise';
import 'core-js/es6/set';
import 'core-js/es6/map';



const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};
const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: [],

}
const defaultMeasuremodel = {
  name: '',
  dimension: [],
  metric: [],
  operator: [],
  type: [],

}
const useStyles = makeStyles(styles);
const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}


export default class Validation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMeasuremodel,
      measureMetaModelUrl: 'http://localhost:55183/measure',
      metaModelRelax: defaultMetamodel,
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      loading: false,
      relaxMetaModelModal: false,
      views: [],
      spreads: [],
      rowids: [],
      filters: [],
      measureMetaModelModal: false,
      dimension: [],
      name_dim: [],
      name_level: [],
      name_metric: [],
      name_attribute: [],
      measure_metric: [],
      attr: [],
      attr_v: [],
      attr_name: [],
      view_name: [],
      measure_name: [],
      measure_agg_method: [],
      attr_dim_dimension: [],
      attr_attr_name: [],
      filter_name: [],
      filter_attr: [],
      filter_attr_name: [],
      filter_attr_dim_dimension: [],
      filter_attr_dim_level: [],
      filter_metric: [],
      filter_function: [],
      spread_name: [],
      spread_source_metric: [],
      spread_target: [],
      rowid_name: [],
      rowid_dimension: [],
      measure_attribute_name:[],
      measure_attribute_dim_dimension:[],
      measure_attribute:[],
    }

  }
  fetchRelax() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.relaxMetaModelUrl)])
      .then(([relaxModelResponse]) => {
        const metaModelRelax = relaxModelResponse.model_response.relax_model;
        this.setState({
          metaModelRelax, loading: false,
          views: metaModelRelax.view,
          filters: metaModelRelax.filter,
          spreads: metaModelRelax.spread,
          rowids: metaModelRelax.row_id_attr,
          measure_metric: metaModelRelax.view.map(measure => measure.measure.map(metric => metric.metric)),
          measure_attribute:metaModelRelax.view.map(measure => measure.measure.map(metric => metric.attribute)),

          measure_agg_method: metaModelRelax.view.map(measure => measure.measure.map(metric => metric.agg_method)),
          attr: metaModelRelax.view.map(measure => measure.attr),
          view_name: metaModelRelax.view.map(view => view.name),
          measure_name: metaModelRelax.view.map(measure => measure.measure.map(metric => metric.name)),
          filter_name: metaModelRelax.filter.map(filter => filter.name),
          filter_attr: metaModelRelax.filter.map(attr => attr.attribute),
          filter_function: metaModelRelax.filter.map(attr => attr.function),
          filter_metric: metaModelRelax.filter.map(attr => attr.metric),
          spread_name: metaModelRelax.spread.map(spread => spread.name),
          spread_source_metric: metaModelRelax.spread.map(spread => spread.source_metric),
          spread_target: metaModelRelax.spread.map(spread => spread.target),
          rowid_name: metaModelRelax.row_id_attr.map(row => row.name),
          rowid_dimension: metaModelRelax.row_id_attr.map(row => row.dimension)



        });
        var mergedattribute = [].concat.apply([], this.state.measure_attribute);
        var mergedattr = [].concat.apply([], this.state.attr);
        var mergedmetric = [].concat.apply([], this.state.measure_metric);
        var mergedagg = [].concat.apply([], this.state.measure_agg_method);
        var mergedname = [].concat.apply([], this.state.measure_name);
        this.setState({ measure_attribute_name:mergedattribute.filter(a => a !== undefined).map(a=>a.name), measure_attribute_dim_dimension:mergedattribute.filter(a => a !== undefined).map(a=>a.dim.dimension), measure_name: mergedname, measure_agg_method: mergedagg, attr: mergedattr, measure_metric: mergedmetric.filter(a => a !== undefined) });
        this.setState({
          attr_v: this.state.attr.filter(a => a !== undefined), filter_attr: this.state.filter_attr.filter(a => a !== undefined)
          , filter_function: this.state.filter_function.filter(a => a !== undefined)
          , filter_metric: this.state.filter_metric.filter(a => a !== undefined)
        });
        console.log("measureattribute",this.state.measure_attribute_name);
        console.log("measureattribute3",this.state.measure_attribute_dim_dimension);
        this.setState({
          attr_name: this.state.attr_v.map(a => a.name), attr_attr_name: this.state.attr_v.map(a => a.attr_name),
          attr_dim_dimension: this.state.attr_v.map(a => a.dim.dimension),
          filter_attr_name: this.state.filter_attr.map(a => a.name),
          filter_attr_dim_dimension: this.state.filter_attr.map(a => a.dim.dimension),
          filter_attr_dim_level: this.state.filter_attr.map(a => a.dim.level)
        })
        console.log("viewname", this.state.measure_attribute_dim_dimension);
        console.log("viewname2", this.state.measure_attribute_name);



        // console.log("attr_attr_name", this.state.attr_attr_name);
        // console.log("attr_name", this.state.attr_name);
        //console.log("dim_dimension", this.state.attr_dim_dimension);
        //console.log("measure_metric",this.state.measure_metric);
        //console.log("measure_agg_method",this.state.measure_agg_method);

      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }
  fetchMeasure() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.measureMetaModelUrl)])
      .then(([measureModelResponse]) => {
        const metaModel = measureModelResponse.model;
        this.setState({
          metaModel, loading: false,
          dimension: metaModel.dimension,
          name_dim: metaModel.dimension.map(dim => dim.name),
          name_level: metaModel.dimension.map(dim => dim.level.map(name => name.name)),
          metric: metaModel.metric,
          name_metric: metaModel.metric.map(metric => metric.name),
          name_attribute: metaModel.dimension.map(dim => dim.level.map(attr => attr.attribute.map(m => m.name))),


        });

        var mergedattribute = [].concat.apply([], this.state.name_attribute);
        var attr = [].concat.apply([], mergedattribute);
        var mergedlevel = [].concat.apply([], this.state.name_level);
        var level = [].concat.apply([], mergedlevel);
        this.setState({ name_attribute: attr, name_level: level });
        console.log("name_attribute", this.state.name_attribute);
        console.log("name_level", this.state.name_level);
        console.log("name_metric", this.state.name_metric);
        console.log("name_dim", this.state.name_dim);




      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })
  }



  componentDidMount() {
    this.fetchMeasure();
    this.fetchRelax()

  }



  render() {






    return (
      <GridContainer>

        <GridItem xs={12} sm={12} md={12}>
          <Card>

            <CardHeader color="danger">
              <h4 className={useStyles.cardTitleWhite}>Views errors</h4>

            </CardHeader>
            <CardBody>
              {this.state.view_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div>
                            <h5>{x} should be type string </h5></div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.measure_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div>
                            <h5>{x} should be type string </h5></div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.measure_agg_method.map((x, i) => {
                return (
                  <div>

                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!['TOTAL', 'COUNT', 'MIN', 'MAX', 'AMBIG', 'AVERAGE', 'COLLECT', 'MODE', 'HISTOGRAM', 'COUNT_DISTINCT'].find(v => v === x)) {
                        return (<div className= "my-notify-error">  {x} is not a dimension  </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}

              {this.state.attr_dim_dimension.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_dim.find(v => v === x)) {
                        return (<div className= "my-notify-error"> {x} is not a dimension </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.measure_attribute_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_attribute.find(v => v === x)) {
                        return (<div className= "my-notify-error">   {x} is not valid </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.measure_attribute_dim_dimension.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_dim.find(v => v === x)) {
                        return (<div  className= "my-notify-error">  {x} is not a dimension </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.attr_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                  </div>)
              })}



              {this.state.attr_attr_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string</div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_attribute.find(v => v === x)) {
                        return (<div  className= "my-notify-error"> {x} is not valid </div>
                        )
                      }
                    })()}
                  </div>)
              })}
              {this.state.measure_metric.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_metric.find(v => v === x)) {
                        return (<div  className= "my-notify-error">  {x} is not a metric </div>
                        )
                      }
                    })()}
                  </div>)
              })}



            </CardBody>
          </Card>


        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card>

            <CardHeader color="danger">
              <h4 className={useStyles.cardTitleWhite}>Filters errors</h4>

            </CardHeader>
            <CardBody>
              {this.state.filter_function.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div>
                            <h5>{x} should be type string </h5></div>
                        )
                      }
                    })()}
                  </div>

                );
              })}


              {this.state.filter_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string</div>
                        )
                      }
                    })()}

                  </div>

                );
              })}
              {this.state.filter_attr_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_attribute.find(v => v === x)) {
                        return (<div  className= "my-notify-error">  {x} is not a true </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.filter_attr_dim_dimension.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_dim.find(v => v === x)) {
                        return (<div  className= "my-notify-error">  {x} is not valid  </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.filter_metric.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_metric.find(v => v === x)) {
                        return (<div  className= "my-notify-error">  {x} is not a metric </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.filter_attr_dim_level.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_level.find(v => v === x)) {
                        return (<div  className= "my-notify-error">   {x} is not a level </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}



            </CardBody>
          </Card>


        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card>

            <CardHeader color="danger">
              <h4 className={useStyles.cardTitleWhite}>Spread errors</h4>

            </CardHeader>
            <CardBody>
              {this.state.spread_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}


              {this.state.spread_source_metric.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_metric.find(v => v === x)) {
                        return (<div  className= "my-notify-error">   {x} is not a metric  </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.spread_target.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_metric.find(v => v === x)) {
                        return (<div  className= "my-notify-error">   {x} is not a metric </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}




            </CardBody>
          </Card>


        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card>

            <CardHeader color="danger">
              <h4 className={useStyles.cardTitleWhite}>Row_Id errors</h4>

            </CardHeader>
            <CardBody>

              {this.state.rowid_name.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_attribute.find(v => v === x)) {
                        return (<div  className= "my-notify-error">   {x} is not a valid  </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}
              {this.state.rowid_dimension.map((x, i) => {
                return (
                  <div>
                    {(() => {
                      if (typeof (x) !== 'string') {
                        return (
                          <div  className= "my-notify-error">
                            {x} should be type string </div>
                        )
                      }
                    })()}
                    {(() => {
                      if (!this.state.name_dim.find(v => v === x)) {
                        return (<div  className= "my-notify-error">   {x} is not a dimension </div>
                        )
                      }
                    })()}
                  </div>

                );
              })}




            </CardBody>
          </Card>


        </GridItem>

      </GridContainer>
    );
  }
}