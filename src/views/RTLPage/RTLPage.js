import React from 'react';
import { Button, Checkbox, Input, Modal, notification } from 'antd';
import Icon from '@ant-design/icons';
import ReactJson from 'react-json-view';
import Dashboard from 'views/Dashboard/Dashboard'

import LoginForm from './LoginForm';

const defaultMetamodel = {
  view: [],
  filter: [],
  level: [],
}

const defaultAuthentication = {
  loginUrl: 'http://localhost:8080/login',
  realm: 'login_auth',
  realmKey: 'realm',
  user: '',
  userKey: 'username',
  password: '',
  passwordKey: 'password',
}

const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),
    credentials: 'include',
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}

const authenticate = ({
  loginUrl,
  realm,
  realmKey,
  user,
  userKey,
  password,
  passwordKey,
}) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(loginUrl, {
    headers,
    method: 'POST',
    body: JSON.stringify({ [realmKey]: realm, [userKey]: user, [passwordKey]: password }),
    credentials: 'include',
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}

export default class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      measureMetaModelUrl: 'http://localhost:55183/measure',
      loading: false,
      relaxMetaModelModal: false,
      authentication: defaultAuthentication,
      enableAuthentication: false,
    
    };
  }

  fetchMetaModel() {

    this.setState({ loading: true });

    if (this.state.enableAuthentication) {
      authenticate(this.state.authentication)
        .then(() => {
          Promise.all([
            getMetaModelPromise(this.state.relaxMetaModelUrl),
            getMetaModelPromise(this.state.measureMetaModelUrl),
          ])
            .then(([relaxModelResponse, measureModelResponse]) => {
              const metaModel = relaxModelResponse.model_response.relax_model;
              metaModel.level = measureModelResponse.model.dimension
              console.log(metaModel);
              this.setState({ Views:metaModel.view, loading: false });
            
            })
        })
        .catch((error) => {
          this.setState({ loading: false });
          notification.error({
            message: error.message,
            description: error.stack,
          })
        })
    } else {
      Promise.all([
        getMetaModelPromise(this.state.relaxMetaModelUrl),
        getMetaModelPromise(this.state.measureMetaModelUrl),
      ])
        .then(([relaxModelResponse, measureModelResponse]) => {
          const metaModel = relaxModelResponse.model_response.relax_model;
          metaModel.level = measureModelResponse.model.dimension
          console.log(metaModel);
          this.setState({ metaModel, loading: false })
        })
        .catch((error) => {
          this.setState({ loading: false });
          notification.error({
            message: error.message,
            description: error.stack,
          })
        })
    }
  }

  handleLoginFormChange(authentication) {
    this.setState(state => ({ authentication: { ...state.authentication, ...authentication } }))
  }

  render() {
    return (
      <div className="App">
        <Modal
          visible={this.state.metaModel === defaultMetamodel}
          width="60%"
          title="Meta-model URLs"
          closable={false}
          centered={true}
          onOk={() => this.fetchMetaModel()}
          confirmLoading={this.state.loading}
        >
          <h4>Relax Meta-model Url</h4>
          <Input
            allowClear
            addonBefore={<Icon type="api" />}
            value={this.state.relaxMetaModelUrl}
            onChange={(e) => this.setState({ relaxMetaModelUrl: e.target.value })}
            style={{ marginBottom: '1rem' }}
          />

          <h4>Measure Meta-model Url</h4>
          <Input
            allowClear
            addonBefore={<Icon type="api" />}
            value={this.state.measureMetaModelUrl}
            onChange={(e) => this.setState({ measureMetaModelUrl: e.target.value })}
            style={{ marginBottom: '1rem' }}
          />
          <Checkbox
            style={{ marginBottom: '1rem' }}
            onChange={(e) => this.setState({ enableAuthentication: e.target.checked })}
          >
            Enable Authentication
          </Checkbox>
          {this.state.enableAuthentication && <LoginForm onChange={(authentication) => this.handleLoginFormChange(authentication)} {...defaultAuthentication} />}
        </Modal>
        <Modal
          visible={this.state.relaxMetaModelModal}
          width="60%"
          title="Relax Meta Model"
          closable={false}
          centered={true}
          onCancel={() => this.setState({ relaxMetaModelModal: false })}
          footer={null}
        >
          <ReactJson src={this.state.metaModel} iconStyle="triangle" theme="flat" collapsed={2} style={{ width: '100%' }} />
        </Modal>
         
       
      </div>
    );
  }
}