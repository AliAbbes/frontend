const filtersToString = (filters) => {
  return filters.map(
    ({ name, op, value }) => {
      if (typeof value === 'string') {

        // Escape '&' and '\' caracters
        const escapedVal = value.replace(/\\/g, '\\\\').replace(/&/g, '\\&');
        return `${name}${op}${escapedVal}`;
      }
      return `${name}${op}${value}`;
    }).join('&')
};

const generateQueryString = (view, levels, filters) => {
  const levels_str = levels && levels.map(({ dimension, level }) => `${dimension}level=${level}`).join('&');
  const filters_str = filters && filtersToString(filters);

  if (levels_str && filters_str) {
    return `/query/${view}?${levels_str}&${filters_str}`;
  }
  if (levels_str) {
    return `/query/${view}?${levels_str}`;
  }
  if (filters_str) {
    return `/query/${view}?${filters_str}`;
  }
  return `/query/${view}`;
};

const generateQueries = (measures) => measures.map(({ view, levels, filters }) => generateQueryString(view, [...levels.values()], [...filters.values()]));

export const generateRequestBody = (measures) => JSON.stringify({ relax_path: generateQueries(measures) });