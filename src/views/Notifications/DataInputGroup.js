import React from 'react';
import { Button } from 'antd';
import Icon from '@ant-design/icons';

export default class DataInputGroup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputFieldKeys: [0],
    }
  }

  addNewField() {
    const newKey = (this.state.inputFieldKeys[this.state.inputFieldKeys.length - 1] || 0) + 1;
    this.setState((state) => ({ inputFieldKeys: [...state.inputFieldKeys, newKey] }));
  }

  removeField(key) {
    this.setState((state) => {
      return ({
        inputFieldKeys: state.inputFieldKeys.filter(fieldKey => fieldKey !== key),
      });
    },
      () => this.props.handleRemove(key),
    );
  }

  render() {
    return (
      <div style={{ flex: '0 0 49%' }} >
        {
          this.state.inputFieldKeys.map((key) => (
            <div key={`div-${key}`} style={{ display: 'flex', alignItems: 'center', marginBottom: '1rem' }}>
              {React.cloneElement(this.props.children, { elementKey: key })}
              <Icon
                key={`icon-${key}`}
                style={{ fontSize: '24px', marginLeft: '0.5rem' }}
                className="delete-button"
                type="minus-circle-o"
                onClick={() => this.removeField(key)}
              />
            </div>
          ))
        }
        <Button icon="plus" type="dashed" onClick={() => this.addNewField()} style={{ width: '100%' }}> More </Button>
      </div>
    );
  }

}
