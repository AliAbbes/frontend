import React from 'react';
import { Tooltip } from 'antd';
import Button from '@material-ui/core/Button';
import ReactJson from 'react-json-view';
import ResultTableModal from './ResultTableModal'
import ViewConfigModal from './ViewConfigModal'


export default class ResultPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableModal: false,
      viewConfigModal: false,
    };
  }

  render() {
    return (
      <div className="result-panel">
        <ResultTableModal
          metaModel={this.props.metaModel}
          responses={this.props.data.relax_response || []}
          visible={this.state.tableModal}
          closeModal={() => this.setState({ tableModal: false })}
        />
        <ViewConfigModal
          metaModel={this.props.metaModel}
          visible={this.state.viewConfigModal}
          views={this.props.views}
          closeModal={() => this.setState({ viewConfigModal: false })}
        />
        <ReactJson src={this.props.data} iconStyle="triangle" theme="flat" collapsed={2} style={{ width: '100%' }} />
        <div className="result_buttons">
          <Tooltip placement="left" title="Show result in table">
            <Button
              shape="circle"
              type="default"
              icon="table"
              onClick={() => this.setState({ tableModal: true })} style={{ marginBottom: '1rem' }}
              disabled={!this.props.data.relax_response}
            />
          </Tooltip>
          <Tooltip placement="left" title="Show selected Views configurations">
            <Button
              shape="circle"
              type="default"
              icon="tool"
              onClick={() => this.setState({ viewConfigModal: true })}
              disabled={!this.props.views.length}
            />
          </Tooltip>
        </div>
      </div>
    );
  }
}