import React from 'react';
import { Tag } from 'antd';
import Popover from '@material-ui/core/Popover';
import Modal from '@material-ui/core/Modal';
import Table from '@material-ui/core/Table';
import Tabs from '@material-ui/core/Tabs';

const parseResponseByMeasure = (el) => {
  if (el.row !== undefined) {
    return el.row.map((elem) => {
      const obj = {};
      elem.cell.forEach((cell) => {
        if (cell.kind === 'MULTI_VALUED') {
          obj[cell.col_name] = [];
          obj[cell.col_name] = Object['values'](cell.mv_cell)[0];
        } else {
          obj[cell.col_name] = Object['values'](cell.sv_cell)[0];
        }
      });
      return obj;
    });
  }
  return [];
}

const getPopoverContent = (elements) => <div>{elements.map(el => <p>{el}</p>)}</div>

const getColumnConfig = (name) => ({
  title: name,
  dataIndex: name,
  render: (value) => {
    if (value === undefined) {
      return <Tag>Undefined</Tag>
    } else if (value === null) {
      return <Tag>Null</Tag>
    } else if (value === true) {
      return <Tag>TRUE</Tag>
    } else if (value === false) {
      return <Tag>FALSE</Tag>
    } else if (Array.isArray(value)) {
      return <Popover content={getPopoverContent(value)}><Tag>Multiple values</Tag></Popover>
    }
    return value;
  }
})

const columnsByView = (response, props) => {
  const view = props.metaModel.view.find(view => view.name === response.view_name);
  const attributes = (view.attr || []).map(attr => getColumnConfig(attr.name));
  const measures = (view.measure || []).map(measure => getColumnConfig(measure.name));
  return [...attributes, ...measures];
}

const ResultTableModal = (props) => (
  <Modal width="80%" centered={true} visible={props.visible} onCancel={() => props.closeModal()} footer={null} >
    <Tabs type="card">
      {(props.responses || []).map(
        (response) => (
          <Tabs.TabPane tab={response.view_name} key={response.relax}>
            <h2> {response.relax} </h2>
            <Table
              bordered
              size="small"
              scroll={{ x: true }}
              columns={columnsByView(response, props)}
              dataSource={parseResponseByMeasure(response)} />
          </Tabs.TabPane>
        )
      )}
    </Tabs>
  </Modal>
)

export default ResultTableModal;