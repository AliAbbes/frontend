import React from 'react';
import { Input, Select } from 'antd';

export default class LevelInputs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedValues: new Map(),
    }
  }

  render() {
    return (

      <Input.Group compact>
        <Select
          showSearch
          key={`Select-${this.props.elementKey}`}
          style={{ width: '50%' }}
          placeholder="Dimension"
          optionFilterProp="children"
          onChange={(dimension) => this.props.handleChange({ dimension, level: '' }, this.props.elementKey)}
          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          getPopupContainer={trigger => trigger.parentNode}
        >
          {
            (this.props.dimensions || []).map((value) => <Select.Option key={value} value={value}>{value}</Select.Option>)
          }
        </Select>

        <Select
          showSearch
          key={`Select-Op-${this.props.elementKey}`}
          style={{ width: '50%' }}
          optionFilterProp="children"
          defaultValue="Level"
          onChange={(level) => this.props.handleChange({ level }, this.props.elementKey)}
          filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
          getPopupContainer={trigger => trigger.parentNode}
        >
          {
            (this.props.levels(this.props.elementKey) || []).map((value) => <Select.Option key={value} value={value}>{value}</Select.Option>)
          }
        </Select>

      </Input.Group>

    );
  }
}
