import React from 'react';
import { Button, Checkbox, Input, Modal, notification } from 'antd';
import ReactJson from 'react-json-view';
import Icon from '@ant-design/icons';
import MainContainer from './MainContainer';
import LoginForm from './LoginForm';
import GridContainer from "components/Grid/GridContainer.js";

const defaultMetamodel = {
  view: [],
  filter: [],
  level: [],
}

const defaultAuthentication = {
  loginUrl: 'http://localhost:8080/login',
  realm: 'login_auth',
  realmKey: 'realm',
  user: '',
  userKey: 'username',
  password: '',
  passwordKey: 'password',
}

const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),
    credentials: 'include',
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}

const authenticate = ({
  loginUrl,
  realm,
  realmKey,
  user,
  userKey,
  password,
  passwordKey,
}) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(loginUrl, {
    headers,
    method: 'POST',
    body: JSON.stringify({ [realmKey]: realm, [userKey]: user, [passwordKey]: password }),
    credentials: 'include',
  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}

export default class RelaxPlay extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      measureMetaModelUrl: 'http://localhost:55183/measure',
      loading: false,
      relaxMetaModelModal: false,
      authentication: defaultAuthentication,
      enableAuthentication: false,
    };
  }

  fetchMetaModel() {

    this.setState({ loading: true });

    if (this.state.enableAuthentication) {
 
      
          Promise.all([
            getMetaModelPromise(this.state.relaxMetaModelUrl),
            getMetaModelPromise(this.state.measureMetaModelUrl),
          ])
            .then(([relaxModelResponse, measureModelResponse]) => {
              const metaModel = relaxModelResponse.model_response.relax_model;
              metaModel.level = measureModelResponse.model.dimension
              console.log(metaModel);
              this.setState({ Views:metaModel.view, loading: false });
         
            })
    
        .catch((error) => {
          this.setState({ loading: false });
          notification.error({
            message: error.message,
            description: error.stack,
          })
        })
    } else {
      Promise.all([
        getMetaModelPromise(this.state.relaxMetaModelUrl),
        getMetaModelPromise(this.state.measureMetaModelUrl),
      ])
        .then(([relaxModelResponse, measureModelResponse]) => {
          const metaModel = relaxModelResponse.model_response.relax_model;
          metaModel.level = measureModelResponse.model.dimension
          console.log(metaModel);
          this.setState({ metaModel, loading: false })
        })
        .catch((error) => {
          this.setState({ loading: false });
          notification.error({
            message: error.message,
            description: error.stack,
          })
        })
    }
  
  }

  handleLoginFormChange(authentication) {
    this.setState(state => ({ authentication: { ...state.authentication, ...authentication } }))
  }

  componentDidMount() {
    this.fetchMetaModel()
  }


  render() {
    return (
  
      <div className="App">
     
         
      
        <MainContainer metaModel={this.state.metaModel} showMetaModel={() => this.setState({ relaxMetaModelModal: true })} />

      </div>
   
    );
  }
}