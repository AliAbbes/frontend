import React from 'react';
import {  Input, Tooltip } from 'antd';
import Icon from '@ant-design/icons';

const LoginForm = (props) => (
  <div>

    <Input
      allowClear
      placeholder="Login Url"
      defaultValue={props.loginUrl}
      addonBefore={<Icon type="api" />}
      onChange={(e) => props.onChange({ loginUrl: e.target.value })}
      style={{ marginBottom: '1rem' }}
    />

    <Input.Group compact style={{ marginBottom: '1rem' }}>
      <Input
        style={{ width: '50%' }}
        placeholder="Realm"
        defaultValue={props.realm}
        addonBefore={<Icon type="global" />}
        onChange={(e) => props.onChange({ realm: e.target.value })}
      />
      <Input
        style={{ width: '50%' }}
        placeholder="Realm Key"
        addonBefore={<Tooltip title="Realm Authentication object key"><Icon type="key" /></Tooltip>}
        defaultValue={props.realmKey}
        onChange={(e) => props.onChange({ realmKey: e.target.value })}
      />
    </Input.Group>

    <Input.Group compact style={{ marginBottom: '1rem' }}>
      <Input
        style={{ width: '50%' }}
        placeholder="User"
        addonBefore={<Icon type="user" />}
        onChange={(e) => props.onChange({ user: e.target.value })}
      />
      <Input
        style={{ width: '50%' }}
        placeholder="Username Key"
        addonBefore={<Tooltip title="Username Authentication object key"><Icon type="key" /></Tooltip>}
        defaultValue={props.userKey}
        onChange={(e) => props.onChange({ userKey: e.target.value })}
      />
    </Input.Group>

    <Input.Group compact style={{ marginBottom: '1rem' }}>
      <Input
        style={{ width: '50%' }}
        type="password"
        placeholder="Password"
        addonBefore={<Icon type="lock" />}
        onChange={(e) => props.onChange({ password: e.target.value })}
      />
      <Input
        style={{ width: '50%' }}
        placeholder="Password Key"
        addonBefore={<Tooltip title="Password Authentication object key"><Icon type="key" /></Tooltip>}
        defaultValue={props.passwordKey}
        onChange={(e) => props.onChange({ passwordKey: e.target.value })}
      />
    </Input.Group>

  </div>
)

export default LoginForm;