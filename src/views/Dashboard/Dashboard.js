
import React from "react";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
import Toc from '@material-ui/icons/Toc';

// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Fingerprint from "@material-ui/icons/Fingerprint";
import FilterList from "@material-ui/icons/FilterList";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core components
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "components/Table/Table.js";
import Tasks from "components/Tasks/Tasks.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from "components/Typography/Danger.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import ReactJson from 'react-json-view';
import logo from './logo.png';


import { bugs, website, server } from "variables/general.js";
import { withStyles } from '@material-ui/core/styles';
import {
  successColor,
  whiteColor,
  grayColor,
  hexToRgb
} from "assets/jss/material-dashboard-react.js";

import {
  dailySalesChart,
  emailsSubscriptionChart,
  completedTasksChart
} from "variables/charts.js";

import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import { Container, Button, Grid } from "@material-ui/core";



const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: [],

}
const defaultMeasuremodel = {
  name: '',
  dimension: [],
  metric: [],
  operator: [],
  type: [],

}
const useStyles = makeStyles(styles);
const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}


export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMeasuremodel,
      measureMetaModelUrl: 'http://localhost:55183/measure',
      metaModelRelax: defaultMetamodel,
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      loading: false,
      relaxMetaModelModal: false,
      views: [],
      spreads: [],
      rowids: [],
      filters: [],
      measureMetaModelModal: false,
      dimension: [],
      name_dim: [],
      name_level: [],
      name_metric: [],
      name_attribute: [],
      measure_metric: [],
      attr: [],
      attr_v: [],
      attr_name: [],
      view_name: [],
      measure_name: [],
      measure_agg_method: [],
      attr_dim_dimension: [],
      attr_attr_name: [],
      filter_name: [],
      filter_attr: [],
      filter_attr_name: [],
      filter_attr_dim_dimension: [],
      filter_attr_dim_level: [],
      filter_metric: [],
      filter_function: [],
      spread_name: [],
      spread_source_metric: [],
      spread_target: [],
      rowid_name: [],
      rowid_dimension: [],
      measure_attribute_name: [],
      measure_attribute_dim_dimension: [],
      measure_attribute: [],
    }

  }
  fetchRelax() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.relaxMetaModelUrl)])
      .then(([relaxModelResponse]) => {
        const metaModelRelax = relaxModelResponse.model_response.relax_model;
        this.setState({
          metaModelRelax, loading: false,
          views: metaModelRelax.view,
          filters: metaModelRelax.filter,
          spreads: metaModelRelax.spread,
          rowids: metaModelRelax.row_id_attr,
          measure_metric: metaModelRelax.view.map(measure => measure.measure.map(metric => metric.metric)),
          measure_attribute: metaModelRelax.view.map(measure => measure.measure.map(metric => metric.attribute)),

          measure_agg_method: metaModelRelax.view.map(measure => measure.measure.map(metric => metric.agg_method)),
          attr: metaModelRelax.view.map(measure => measure.attr),
          view_name: metaModelRelax.view.map(view => view.name),
          measure_name: metaModelRelax.view.map(measure => measure.measure.map(metric => metric.name)),
          filter_name: metaModelRelax.filter.map(filter => filter.name),
          filter_attr: metaModelRelax.filter.map(attr => attr.attribute),
          filter_function: metaModelRelax.filter.map(attr => attr.function),
          filter_metric: metaModelRelax.filter.map(attr => attr.metric),
          spread_name: metaModelRelax.spread.map(spread => spread.name),
          spread_source_metric: metaModelRelax.spread.map(spread => spread.source_metric),
          spread_target: metaModelRelax.spread.map(spread => spread.target),
          rowid_name: metaModelRelax.row_id_attr.map(row => row.name),
          rowid_dimension: metaModelRelax.row_id_attr.map(row => row.dimension)



        });
        var mergedattribute = [].concat.apply([], this.state.measure_attribute);
        var mergedattr = [].concat.apply([], this.state.attr);
        var mergedmetric = [].concat.apply([], this.state.measure_metric);
        var mergedagg = [].concat.apply([], this.state.measure_agg_method);
        var mergedname = [].concat.apply([], this.state.measure_name);
        this.setState({ measure_attribute_name: mergedattribute.filter(a => a !== undefined).map(a => a.name), measure_attribute_dim_dimension: mergedattribute.filter(a => a !== undefined).map(a => a.dim.dimension), measure_name: mergedname, measure_agg_method: mergedagg, attr: mergedattr, measure_metric: mergedmetric.filter(a => a !== undefined) });
        this.setState({
          attr_v: this.state.attr.filter(a => a !== undefined), filter_attr: this.state.filter_attr.filter(a => a !== undefined)
          , filter_function: this.state.filter_function.filter(a => a !== undefined)
          , filter_metric: this.state.filter_metric.filter(a => a !== undefined)
        });
        console.log("measureattribute", this.state.measure_attribute_name);
        console.log("measureattribute3", this.state.measure_attribute_dim_dimension);
        this.setState({
          attr_name: this.state.attr_v.map(a => a.name), attr_attr_name: this.state.attr_v.map(a => a.attr_name),
          attr_dim_dimension: this.state.attr_v.map(a => a.dim.dimension),
          filter_attr_name: this.state.filter_attr.map(a => a.name),
          filter_attr_dim_dimension: this.state.filter_attr.map(a => a.dim.dimension),
          filter_attr_dim_level: this.state.filter_attr.map(a => a.dim.level)
        })
        console.log("viewname", this.state.measure_attribute_dim_dimension);
        console.log("viewname2", this.state.measure_attribute_name);



        // console.log("attr_attr_name", this.state.attr_attr_name);
        // console.log("attr_name", this.state.attr_name);
        //console.log("dim_dimension", this.state.attr_dim_dimension);
        //console.log("measure_metric",this.state.measure_metric);
        //console.log("measure_agg_method",this.state.measure_agg_method);

      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }
  fetchMeasure() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.measureMetaModelUrl)])
      .then(([measureModelResponse]) => {
        const metaModel = measureModelResponse.model;
        this.setState({
          metaModel, loading: false,
          dimension: metaModel.dimension,
          name_dim: metaModel.dimension.map(dim => dim.name),
          name_level: metaModel.dimension.map(dim => dim.level.map(name => name.name)),
          metric: metaModel.metric,
          name_metric: metaModel.metric.map(metric => metric.name),
          name_attribute: metaModel.dimension.map(dim => dim.level.map(attr => attr.attribute.map(m => m.name))),


        });

        var mergedattribute = [].concat.apply([], this.state.name_attribute);
        var attr = [].concat.apply([], mergedattribute);
        var mergedlevel = [].concat.apply([], this.state.name_level);
        var level = [].concat.apply([], mergedlevel);
        this.setState({ name_attribute: attr, name_level: level });
        console.log("name_attribute", this.state.name_attribute);
        console.log("name_level", this.state.name_level);
        console.log("name_metric", this.state.name_metric);
        console.log("name_dim", this.state.name_dim);




      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })
  }



  componentDidMount() {
    this.fetchMeasure();
    this.fetchRelax()

  }



  render() {





    return (<div>
      <GridContainer>
      <GridItem xs={12} sm={6} md={3}>
        <Card>
          <CardHeader stats icon>
            <CardIcon color="info">
            <img src={logo} alt="logo" /> 
            </CardIcon>
        
          </CardHeader>
          <CardFooter stats>
            <div className={useStyles.stats}>

            <Button   href="http://localhost:1234 "  variant="outlined" color="secondary">
                  Relax Play-ground
     
      </Button>
              <Container>
                
              </Container>
            </div>
          </CardFooter>
        </Card>
      </GridItem>

      </GridContainer>  <GridContainer>

      <GridItem xs={12} sm={6} md={3}>
        <Card>
          <CardHeader color="warning" stats icon>
            <CardIcon color="warning">
              <Icon>content_copy</Icon>
            </CardIcon>
            <a>View</a>
            <h3 color="warning" >
              49/50 <small>GB</small>
            </h3>
          </CardHeader>
          <CardFooter stats>
            <div className={useStyles.stats}>

              <h3>Views Number:</h3>
              <Container>
                <h4 color="warning" >
                  {this.state.views.length} <small>view</small>
                </h4>
              </Container>
            </div>
          </CardFooter>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={6} md={3}>
        <Card>
          <CardHeader color="success" stats icon>
            <CardIcon color="success">
              <FilterList/>
            </CardIcon>
          
          </CardHeader>
          <CardFooter stats>
          <div className={useStyles.stats}>

          <h3>Filters Number:</h3>
              <Container>
                <h4 color="warning" >
                  {this.state.filters.length} <small>filter</small>
                </h4>
              </Container>
              </div>
          </CardFooter>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={6} md={3}>
        <Card>
          <CardHeader color="danger" stats icon>
            <CardIcon color="danger">
              <Toc/>
            </CardIcon>
          
          </CardHeader>
          <CardFooter stats>
            <div className={useStyles.stats}>
         
              <h3>Spreads Number:</h3>
              <Container>
                <h4 color="warning" >
                  {this.state.spreads.length} <small>spread</small>
                </h4>
              </Container>
            </div>
          </CardFooter>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={6} md={3}>
        <Card>
          <CardHeader color="info" stats icon>
            <CardIcon color="info">
              <Fingerprint />
            </CardIcon>
       
          </CardHeader>
          <CardFooter stats>
            <div className={useStyles.stats}>
            <h3>Row Ids Number:</h3>
              <Container>
                <h4 color="warning" >
                  {this.state.rowids.length} <small>row id</small>
                </h4>
              </Container>
            </div>
          </CardFooter>
        </Card>
      </GridItem>
    </GridContainer>
    <GridContainer>
       
    <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="primary">
              <h4 className={useStyles.cardTitleWhite}>Relax MetaModel</h4>

            </CardHeader>
            <CardBody>

          
            <ReactJson src={this.state.metaModelRelax} />
            </CardBody>
          </Card>

        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="primary">
              <h4 className={useStyles.cardTitleWhite}>Measure MetaModel</h4>

            </CardHeader>
            <CardBody>

          
            <ReactJson src={this.state.metaModel} />
            </CardBody>
          </Card>

        </GridItem>

    </GridContainer>
  </div>
    );
  }
}