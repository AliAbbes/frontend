import React, { Component } from 'react';
import { makeStyles } from "@material-ui/core/styles";

// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import { Link } from "react-router-dom";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { Table } from 'react-bootstrap';
import Icon from "@material-ui/core/Icon";
import axios from 'axios';
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import Accessibility from "@material-ui/icons/Accessibility";
import CardIcon from "components/Card/CardIcon.js";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
import Warning from "@material-ui/icons/Warning";
import { Container } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
const useStyles = makeStyles(styles);




const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: [],

};
const defaultMeasuremodel = {
  name: '',
  dimension: [],
  metric: [],
  operator: [],
  type: [],

}

const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}




export default class EditView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      measureMetaModelUrl: 'http://localhost:55183/measure',
      editviews: [],
      allViews: [],
      edit: [],
      loading: false,
      relaxMetaModelModal: false,
      views: [],
      attr: [],
     
      allMeasure:[],
      measure: { name: "", agg_method: "" },
      metuelleOject:{},
      allUpdate:[],

      name: '',
      nameError: '',
      descriptionError: '',
      description: '',
      ad_hoc: '',
      selectedOption: '',
      validator: '',
      measureModel: defaultMeasuremodel,
      dimension: [],
      name_dim: [],
      name_level: [],
      name_metric: [],
      name_attribute: [],
      name_attrError: '',
      dimError: '',
      metricError: '',
      attributeNameError: '',
      attributeDimError: '',
      metric: [],
      dimAttrError: "",
      nameAttrError: ""

    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAddClickAttr = this.handleAddClickAttr.bind(this);
    this.handleAddClickMeasureAttribute = this.handleAddClickMeasureAttribute.bind(this);
    this.handleAddClickMeasureMetric = this.handleAddClickMeasureMetric.bind(this);
    this.handleAddClickMeasureAdhoc = this.handleAddClickMeasureAdhoc.bind(this);


  }

  fetchMeasure() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.measureMetaModelUrl)])
      .then(([measureModelResponse]) => {
        const measureModel = measureModelResponse.model;
        this.setState({
          measureModel, loading: false,
          name_dim: measureModel.dimension.map(dim => dim.name),
          name_level: measureModel.dimension.map(dim => dim.level.map(name => name.name)),
          name_metric: measureModel.metric.map(metric => metric.name),
          name_attribute: measureModel.dimension.map(dim => dim.level.map(attr => attr.attribute.map(m => m.name))),


        });

        var mergedattribute = [].concat.apply([], this.state.name_attribute);
        var attr = [].concat.apply([], mergedattribute);
        var mergedlevel = [].concat.apply([], this.state.name_level);
        var level = [].concat.apply([], mergedlevel);
        this.setState({ name_attribute: attr, name_level: level });
        console.log("name_attribute", this.state.name_attribute);
        console.log("name_level", this.state.name_level);
        console.log("name_metric", this.state.name_metric);
        console.log("name_dim", this.state.name_dim);




      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })
  }


  fetchview() {

    this.setState({ loading: true });

    console.log(this.props);

    Promise.all([
      getMetaModelPromise(this.state.relaxMetaModelUrl),

    ])
      .then(([relaxModelResponse]) => {
        const metaModel = relaxModelResponse.model_response.relax_model;


        this.setState({
          metaModel, loading: false,
          views: metaModel.view.filter(view => view.name == this.props.match.params.viewname),
          allViews: metaModel.view,
          name: metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].name,
          description: metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].description,
          extension_point: metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].extension_point,
          allMeasure: metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].measure,
          attr: metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].attr === undefined?  []:metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].attr,
          attribute: metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].measure.map(a => a.attribute),
          metric: metaModel.view.filter(view => view.name == this.props.match.params.viewname)[0].measure.map(attr => attr.metric),


        });


        var mergedattribute = [].concat.apply([], this.state.attribute);
        this.setState({ attribute: mergedattribute.filter(a => a !== undefined) });
        console.log("attribute", this.state.attribute);


      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }

  editView(newView) {
    const editviews = this.state.allViews.filter(view => view.name !== this.props.match.params.viewname);

    const edit = [...editviews, newView];
    this.setState({ edit });

    console.log('edit view',edit)

    const apiUrl = 'http://localhost:55183/frontend/update-relax-model';
    const dataUrl = 'http://localhost:55183/update-relax-model';

    const headers = new Headers();
    const options = {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ ...this.state.metaModel, view: edit })
    }
    Promise.all([fetch(apiUrl, options), fetch(dataUrl, options)])
      .then(
        (result) => {
          console.log('response',result)
          this.setState({
            response: result,
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }

  
 handleInputChangeMeasure(e, index) {
    const { name, value } = e.target;

    let allMeasure = this.state.allMeasure
    const metuelleOject = this.state.metuelleOject
    const allUpdate = this.state.allUpdate

    let updatedObject

    const ln = allMeasure.length

    

    const noAddObject = (Object.keys(metuelleOject).length === 0 && metuelleOject.constructor === Object) 
   
    
    if(!noAddObject && index===ln-1)
    {
       updatedObject = metuelleOject  

    }
    else
    {
       updatedObject = allMeasure[index]

       
    }

    if (updatedObject.hasOwnProperty('attribute'))
    {
      if(name === 'name_attr')
      {
        updatedObject['attribute']['name'] = value
      }
      else if (name === 'dimension')
      {
        updatedObject['attribute']['dim']['dimension'] = value
      }
      else
      {
        updatedObject[name] = value
      }
    }
    else
    {
      updatedObject[name] = value
    }

    allUpdate.push({obj:updatedObject,idx:index})

    this.setState({
       allUpdate
    })


  }

  handleRemoveClickMeasure(index) {
    const list = this.state.allMeasure
    list.splice(index, 1);
    this.setState({ allMeasure: list });
  }


  handleAddClickMeasureAdhoc(event) {
    const adhocObject = {...this.state.measure,ad_hoc:""}
    const allMeasure = this.state.allMeasure
    
    allMeasure.push(adhocObject)

    this.setState({ 
      metuelleOject: adhocObject,
      allMeasure
    });

    event.preventDefault();
  }


  handleAddClickMeasureAttribute(event) {
    const attributeObject = { ...this.state.measure,attribute:{name:'',dim:{dimension:''}}}
    const allMeasure = this.state.allMeasure
    
    allMeasure.push(attributeObject)

    this.setState({
      metuelleOject: attributeObject,
      allMeasure
    });

    event.preventDefault();
  }


  handleAddClickMeasureMetric(event) {
    const metricObject = { ...this.state.measure,metric:""};
    const allMeasure = this.state.allMeasure
    
    allMeasure.push(metricObject)
    
    this.setState({ 
      metuelleOject: metricObject,
      allMeasure
     });
    
     event.preventDefault();
  }

  handleInputChangeAttr(e, index) {
    console.log("e", index);
    const { name, value } = e.target;
    const attr= this.state.attr;
    attr[index][name] = value;
    this.setState({ attr });
  }

   
  handleRemoveClickAttr(index) {
    const list = [...this.state.attr];
    console.log('list',list.length)
    list.splice(index, 1);
    console.log('list list ',list.length)
    this.setState({ attr: list });
  }


  newListAttr(attr) {
    if (!attr) {
      return [{ name: "", attr_name: "", dim: { dimension: "" } }];
    } else {
      return [...this.state.attr, { name: "", attr_name: "", dim: { dimension: "" } }];
    }
  }


  handleAddClickAttr(event) {
    console.log('hello from add')

    const list = this.newListAttr(this.state.attr);


    this.setState({ attr: list });
    event.preventDefault();
  }

  validate() {
    console.log('Hello from validate')
    let dimError = "";
    let name_attrError = "";
    let metricError = "";
    let nameAttrError = "";
    let dimAttrError = "";

    const attr = this.state.attr

    if (attr !== undefined) {
      this.state.attr.map(x => {
        if (!this.state.name_dim.find(v => v === x.dim.dimension)) { dimError = "dimension is not valid"; }
      }
      )
    }
    if (dimError) {
      this.setState({ dimError });
      return false;
    }

    if (this.state.attr !== undefined) {
      this.state.attr.map(x => {
        if (!this.state.name_attribute.find(v => v === x.attr_name)) { name_attrError = "name_attr is not valid"; }
      }
      )
    }

    if (name_attrError) {
      this.setState({ name_attrError });
      return false;
    }

    
    const allUpdate= this.state.allUpdate
    const allMeasure = this.state.allMeasure
    const ln = allUpdate.length

    for(let i=0;i<ln;i++)
    {
      const {obj,idx} = allUpdate[i]

          if(obj.attribute!== undefined)
          {
              const { attribute:{name,dim:{dimension}} } = obj

              if(! this.state.name_attribute.includes(name))
                {
                  this.setState({nameAttrError:"Name is not valid"})
                  return false
                }
              else
              {
                this.setState({nameAttrError:''})
              }

              if(!this.state.name_dim.includes(dimension))
              {
                this.setState({dimAttrError:"Dimension is not valid"})
                return false
              }
              else
              {
                this.setState({dimAttrError:" "})
              }
          }

          if(obj.metric !== undefined)
          {
              const {metric} = obj
              if(!this.state.name_metric.includes(metric))
              {
                this.setState({metricError: "Metric is not valid"})
                return false
              }
              else
              {
                this.setState({metricError: ""})
              }
          }

          allMeasure[idx] = obj
      
    }

    this.setState({allMeasure})


    return true;
  }

  onSubmit(e) {
    e.preventDefault();
    const attr = this.state.attr;
    const allMeasure = this.state.allMeasure;

    const newView = {
      name: this.refs.name.value,
      description: this.refs.description.value,
      extension_point: this.refs.extension_point.value,
      attr,
      measure:allMeasure,
    }

    const isValid = this.validate();
    console.log('isValid',isValid)

    if (isValid) {
      
      console.log('attr_',this.state.attr)
      this.editView(newView);
    }


    console.log("name", this.state.nameAttrError);

  }

  handleInputChange(e) {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  componentDidMount() {
    this.fetchview();
    this.fetchMeasure();


  }


  render() {




    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4} >
            <Card>
              <CardHeader color="warning" stats icon>
              <CardIcon color="warning">
              <Icon>content_copy</Icon>
            </CardIcon>
                <p className={useStyles.cardCategory}>{this.props.match.params.viewname}</p>
              </CardHeader>

              <CardBody>
                <Table striped bordered hover>
                  <thead>
                    <tr>
                      <th >Description</th>
                      <th >Extension point</th>

                    </tr>
                  </thead>
                  <tbody>
                    {this.state.views.map(view =>
                      <tr key={view.name}>
                        <td>{view.description}</td>
                        <td> {view.extension_point}
                        </td>

                      </tr>
                    )}
                  </tbody>
                </Table>
                <Link className="btn btn-2 btn-sep icon-cart" to="/admin/user">Back</Link>
              </CardBody>

            </Card>
          </GridItem>

          <GridItem xs={12} sm={12} md={8} >
            <Card>
              <CardHeader color="success">
                <h4 className={useStyles.cardTitleWhite}>Edit {this.props.match.params.viewname}</h4>

              </CardHeader>
              <CardBody>
                <div>
                  <br />

                  <form onSubmit={this.onSubmit.bind(this)} >


                    <label htmlFor="name" className="label">Name:
                  <input disabled type="texta" name="name" ref="name" value={this.state.name} onChange={this.handleInputChange} /></label>



                    <label htmlFor="description">Description :
                      <input type="texta" name="description" ref="description" value={this.state.description} onChange={this.handleInputChange} />

                    </label>

                    <label htmlFor="extension_point">Extension point:
                      <input type="texta" name="extension_point" ref="extension_point" value={this.state.extension_point} onChange={this.handleInputChange} />
                    </label>
                    <div>      <label>Attribute
                      {console.log('attr attr attr',this.state.attr)}
                    <div>{this.state.attr.length===0? <div> 

                        <div className="btn-box">

                          <Fab size="small" onClick={event => this.handleAddClickAttr(event)} color="primary" aria-label="add">
                            <AddIcon />
                          </Fab>
                        </div></div> : <div>
                          <Container>
                            {this.state.attr.map((x, i) => {
                              return (
                                <div className="box" >
                                  <label htmlFor="name">Name:
                              <input
                                      type="texta"

                                      name="name"
                                      value={x.name}
                                      onChange={e => this.handleInputChangeAttr(e, i)}

                                    />
                                  </label>

                                  <label htmlFor="attr_name">Attribute Name :
                                <input
                                      type="texta"
                                      name="attr_name"
                                      value={x.attr_name}
                                      onChange={e => this.handleInputChangeAttr(e, i)}

                                    />
                                    
                                    {this.state.name_attrError ?
                                      <div className= "my-notify-error">{this.state.name_attrError}</div> : null}
                                  </label>

                                  <label htmlFor="dimension">Dimension :
                                <input

                                      type="texta"
                                      value={x.dim.dimension}
                                      onChange={e => this.handleInputChangeAttr({ ...e, target: { ...e.target, name: "dim", value: { dimension: e.target.value } } }, i)}
                                    />
                                    {this.state.dimError ?
                                      <div className= "my-notify-error">{this.state.dimError}

                                      </div> : null}
                                    
                                  </label>
<label>   <IconButton aria-label="delete" onClick={() => this.handleRemoveClickAttr(i)}>
                                      <DeleteIcon fontSize="small" />
                                    </IconButton> </label>
                                  <div className="btn-box">
                                    {this.state.attr.length - 1 === i && <Fab size="small" onClick={event => this.handleAddClickAttr(event)} color="primary" aria-label="add">
                                      <AddIcon />
                                    </Fab>}
                                  



                                  </div>
                                </div>
                              );
                            })}
                          </Container>
                        </div>}</div>

                    </label>
                    </div>

                    <div>
                      <label>Measure:
                        <Container>

                          {this.state.allMeasure.map((x, i) => {
                            return (
                              <div className="box" >
                                <label htmlFor="name">Name :
                            <input
                                    type="texta"
                                    name="name"
                                    value={x.name}
                                    onChange={e => this.handleInputChangeMeasure(e, i)}
                                  />
                                </label>

                                <label htmlFor="agg_method">Aggregation Method :
                                  <select name="agg_method" value={x.agg_method} onChange={e => this.handleInputChangeMeasure(e, i)}>
                                    <option value="0" >Select Method :</option>
                                    <option value="COUNT">COUNT</option>
                                    <option value="MIN">MIN</option>
                                    <option value="MAX">MAX</option>
                                    <option value="AMBIG">AMBIG</option>
                                    <option value="AVERAGE">AVERAGE</option>
                                    <option value="COLLECT">COLLECT</option>
                                    <option value="TOTAL">TOTAL</option>
                                    <option value="MODE">MODE</option>
                                    <option value="HISTOGRAM">HISTOGRAM</option>
                                    <option value="COUNT_DISTINCT">COUNT_DISTINCT</option>
                               
                                  </select>
                                 
                                </label>

                               
                                  {(() => {
                                    if (x.metric !== undefined) {
                                      return (
                                        
                                          <label htmlFor="metric"> Metric :
                                          <input 
                                          type="texta"
                                            name="metric"
                                            value={x.metric}
                                            onChange={e => this.handleInputChangeMeasure(e, i)} />
                                          {this.state.metricError ?
                                            <div  className= "my-notify-error">{this.state.metricError}</div> : null}
                                            </label>
                                      
                                      )
                                    } else if (x.ad_hoc !== undefined) {
                                      return (
                                        <label htmlFor="ad_hoc"> Ad_hoc :
                                    
                                          <input
                                          type="texta"
                                            name="ad_hoc"
                                            value={x.ad_hoc}
                                            onChange={e => this.handleInputChangeMeasure(e, i)} />
                                            </label>
                                      )
                                    } else if (x.attribute !== undefined) {
                                      return (
                                        <label htmlFor="attribute"> Attribute :
                                        <Container>
                                          <label htmlFor="name"> Name :
                                         <input
                                          type="texta"
                                          name="name_attr"
                                          value={x.attribute.name}
                                          onChange={e => this.handleInputChangeMeasure(e, i)} />

                                          {this.state.nameAttrError ?
                                            <div  className= "my-notify-error">{this.state.nameAttrError}</div> : null}
                                            </label>
                              <label htmlFor="dimension"> Dimension :
                                          <input
                                            type="texta"
                                            name="dimension"
                                            value={x.attribute.dim.dimension}
                                            onChange={e => this.handleInputChangeMeasure(e, i)} />

                                          {this.state.dimAttrError ?
                                            <div className= "my-notify-error">{this.state.dimAttrError}</div> : null}
                                            </label>
                                            </Container>

                                            </label>
                                        

                                      )
                                    }
                                  })()}
                            {this.state.allMeasure.length - 1 === i && <label>     <div className="btn-box">
                                  <IconButton aria-label="delete" onClick={() => this.handleRemoveClickMeasure(i)}>
                                          <DeleteIcon fontSize="small" />
                                    </IconButton>
                                  </div>
                                  </label>}
<div>

                            {this.state.allMeasure.length - 1 === i && <label> New Measure :                        
                                {this.state.allMeasure.length - 1 === i &&
                                  <select onChange={(e) => { this.setState({ selectedOption: e.target.value }) }}>
                                    <option value ="0">Select Type :</option>
                                    <option value="ad_hoc">Ad_hoc</option>
                                    <option value="metric">Metric</option>
                                    <option value="attribute">Attribute</option>
                                  </select>}
                                </label> }
                                </div>
                                {(() => {
                                  if (this.state.selectedOption === "metric") {
                                    return (
                                      <div className="btn-box">


                                        {this.state.allMeasure.length - 1 === i && <Fab size="small" onClick={event => this.handleAddClickMeasureMetric(event)} color="primary" aria-label="add">
                                          <AddIcon />
                                        </Fab>}
                                        <label>
                                        <IconButton aria-label="delete" onClick={() => this.handleRemoveClickMeasure(i)}>
                                          <DeleteIcon fontSize="small" />
                                        </IconButton>
                                        </label>


                                      </div>
                                    )

                                  }
                                  else if (this.state.selectedOption === "ad_hoc") {
                                    return (
                                      <div className="btn-box">

                                        {this.state.allMeasure.length - 1 === i && <Fab size="small" onClick={event => this.handleAddClickMeasureAdhoc(event)} color="primary" aria-label="add">
                                          <AddIcon />
                                        </Fab>}
                                        <label>
                                        <IconButton aria-label="delete" onClick={() => this.handleRemoveClickMeasure(i)}>
                                          <DeleteIcon fontSize="small" />
                                        </IconButton>
                                        </label>

                                      </div>
                                    )

                                  }
                                  else if (this.state.selectedOption === "attribute") {
                                    return (
                                      <div className="btn-box">
                                        {this.state.allMeasure.length - 1 === i && <Fab size="small" onClick={event => this.handleAddClickMeasureAttribute(event)} color="primary" aria-label="add">
                                          <AddIcon />
                                        </Fab>}
                                        <label>
                                        <IconButton aria-label="delete" onClick={() => this.handleRemoveClickMeasure(i)}>
                                          <DeleteIcon fontSize="small" />
                                        </IconButton>
                                        </label>

                                      </div>
                                    )

                                  }
                                })()}
                              </div>
                            );
                          })}
                        </Container>
                      </label> 
                    </div>
                    <button type="submit" value="Save" className="btn btn-4 btn-sep icon-send" >Save</button>
                  </form>
                </div>

              </CardBody>
            </Card>

          </GridItem >
        </GridContainer>
      </div>

    );
  }

}



