import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, ButtonToolbar } from 'react-bootstrap';
import { Link } from "react-router-dom";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { Table } from 'react-bootstrap';
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import DeleteIcon from '@material-ui/icons/Delete';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import KeyboardVoiceIcon from '@material-ui/icons/KeyboardVoice';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import AlarmIcon from '@material-ui/icons/Alarm';
import Info from '@material-ui/icons/Info';

import DeleteForever from '@material-ui/icons/DeleteForever';
const styles = {


  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);
const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: []

}
const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}

function searchingFor(term) {
  return function (x) {
    return x.name.toLowerCase().includes(term.toLowerCase()) || !term;
  }
}


export default class UserProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      loading: false,
      relaxMetaModelModal: false,
      views: [],

      error: null,
      response: {},
      term: '',

      sortType: 'asc',


    }
    this.searchHandler = this.searchHandler.bind(this);

  }

  fetchMetaModel() {
    this.setState({ loading: true });
    Promise.all([
      getMetaModelPromise(this.state.relaxMetaModelUrl),

    ])
      .then(([relaxModelResponse]) => {

        const metaModel = relaxModelResponse.model_response.relax_model;
        this.setState({
          metaModel, loading: false,
          views: metaModel.view,
        });

      })
      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }


  deleteView(viewname) {
    const views = this.state.views.filter(view => view.name !== viewname);
    this.setState({ views });
    const apiUrl = 'http://localhost:55183/frontend/update-relax-model';
    const dataUrl = 'http://localhost:55183/update-relax-model';
    const headers = new Headers();
    const options = {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ ...this.state.metaModel, view: views })
    }
    Promise.all([fetch(apiUrl, options), fetch(dataUrl, options)])
      .then(
        (result) => {
          this.setState({
            response: result,
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }



  searchHandler(event) {
    this.setState({ term: event.target.value })
  }




  componentDidMount() {
    this.fetchMetaModel()
  }

  render() {

    const { sortType, views } = this.state;

    const sortedView = this.state.views.sort((a, b) => {
      const isReversed = (sortType === 'asc') ? 1 : -1;
      return isReversed * a.name.localeCompare(b.name)
    });


    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={useStyles.cardTitleWhite}>Views</h4>

            </CardHeader>
            <CardBody>

              <div className="searchBox" >
                <input className="searchInput" aria-label="Search" placeholder="Type Here..." type="text" onChange={this.searchHandler} value={this.state.term} />
                <button className="searchButton" href="#">
                  <i className="material-icons">
                    search
                </i>
                </button>
              </div>

              <Table striped bordered hover>

                <thead>
                  <tr>
                    <th >View Name</th>
                    <th >Actions</th>

                  </tr>
                </thead>
                <tbody>

                  {sortedView.filter(searchingFor(this.state.term)).map(view =>
                    <tr key={view.name}>
                      <td>{view.name}</td>
                      <td>
                        <ButtonToolbar><Link to={'/editView/' + view.name} name={view.name} ><IconButton><Info/></IconButton></Link>

                          <IconButton aria-label="delete"  onClick={() => this.deleteView(view.name)} color="secondary">
                            <DeleteForever/>
                          </IconButton>
                        </ButtonToolbar></td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </GridItem>


      </GridContainer>
    );
  }
}