import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, ButtonToolbar } from 'react-bootstrap';
import { Link } from "react-router-dom";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { Table } from 'react-bootstrap';
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import DeleteIcon from '@material-ui/icons/Delete';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import KeyboardVoiceIcon from '@material-ui/icons/KeyboardVoice';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import AlarmIcon from '@material-ui/icons/Alarm';
import Info from '@material-ui/icons/Info';

import DeleteForever from '@material-ui/icons/DeleteForever';
const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);
const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: []

}
const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}


function searchingForSpread(terms) {
  return function (x) {
    return x.name.toLowerCase().includes(terms.toLowerCase()) || !terms;
  }
}


export default class TableList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      loading: false,
      relaxMetaModelModal: false,

      spreads: [],

      error: null,
      response: {},

      terms: '',
      sortType: 'asc',
      spreadname: ''

    }

    this.searchHandlers = this.searchHandlers.bind(this);

  }

  fetchMetaModel() {
    this.setState({ loading: true });
    Promise.all([
      getMetaModelPromise(this.state.relaxMetaModelUrl),

    ])
      .then(([relaxModelResponse]) => {

        const metaModel = relaxModelResponse.model_response.relax_model;
        this.setState({
          metaModel, loading: false,
          spreads: metaModel.spread,
        });

      })
      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }


  
  deleteSpread(spreadname) {
    const spreads = this.state.spreads.filter(spread => spread.name !== spreadname);
    this.setState({ spreads });
    const apiUrl = 'http://localhost:55183/frontend/update-relax-model';
    const dataUrl = 'http://localhost:55183/update-relax-model';
    const headers = new Headers();
    const options = {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ ...this.state.metaModel, spread: spreads })
    }
    Promise.all([fetch(apiUrl, options), fetch(dataUrl, options)])
      .then(
        (result) => {
          this.setState({
            response: result,
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }
 
  searchHandlers(event) {
    this.setState({ terms: event.target.value })
  }




  componentDidMount() {
    this.fetchMetaModel()
  }

  render() {

    const { sortType, spreads, } = this.state;

    
    const sortedSpread = this.state.spreads.sort((a, b) => {
      const isReversed = (sortType === 'asc') ? 1 : -1;
      return isReversed * a.name.localeCompare(b.name)
    });

    

    return (
      <GridContainer>
       
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={useStyles.cardTitleWhite}>Spread</h4>
            </CardHeader>
            <CardBody>

              <div className="searchBox" >
                <input className="searchInput" aria-label="Search" placeholder="Type Here..." type="text" onChange={this.searchHandlers} value={this.state.terms} />
                <button className="searchButton" href="#">
                  <i className="material-icons">
                    search
                </i>
                </button>
              </div>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th >Spread Name</th>
                    <th >Target</th>
                    <th >Source</th>
                    <th >Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {sortedSpread.filter(searchingForSpread(this.state.terms)).map(spread =>
                    <tr key={spread.name}>
                      <td>{spread.name}</td>
                      <td>{spread.target} </td>
                      <td>{spread.source_metric} </td>
                      <td> <ButtonToolbar>   <Link to={'/editSpread/' + spread.name} nam={spread.name} ><IconButton ><Info/></IconButton></Link>
                     
                          <IconButton aria-label="delete"  onClick={() => this.deleteSpread(spread.name)} color="secondary">
                            <DeleteForever />
                          </IconButton>
                      </ButtonToolbar></td>
                    </tr>
                  )}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        </GridItem>
        
       

      </GridContainer>
    );
  }
}