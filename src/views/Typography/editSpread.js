import React, { Component } from 'react';
import { makeStyles } from "@material-ui/core/styles";

// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import { Link } from "react-router-dom";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { Table } from 'react-bootstrap';
import axios from 'axios';
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import Accessibility from "@material-ui/icons/Accessibility";
import CardIcon from "components/Card/CardIcon.js";
import Toc from '@material-ui/icons/Toc';

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
import Warning from "@material-ui/icons/Warning";
import { Container } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
const defaultMeasuremodel = {
  name: '',
  dimension: [],
  metric: [],
  operator: [],
  type: [],

}
const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};
const useStyles = makeStyles(styles);
const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: [],

}
const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}


export default class EditSpread extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      measureMetaModelUrl: 'http://localhost:55183/measure',
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      loading: false,
      relaxMetaModelModal: false,
      spreads: [],
      name: '',
      description: '',
      extension_point: '',
      source_metric: "",
      target: "",
      editspreads: [],
      allSpreads: [],
      edit: [],
      measureModel: defaultMeasuremodel,
      dimension: [],
      name_dim: [],
      name_level: [],
      name_metric: [],
      name_attribute: [],
      source_metricError: '',
      targetError: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }
  fetchMeasure() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.measureMetaModelUrl)])
      .then(([measureModelResponse]) => {
        const measureModel = measureModelResponse.model;
        this.setState({
          measureModel, loading: false,
          dimension: measureModel.dimension,
          name_dim: measureModel.dimension.map(dim => dim.name),
          name_level: measureModel.dimension.map(dim => dim.level.map(name => name.name)),
          metric: measureModel.metric,
          name_metric: measureModel.metric.map(metric => metric.name),
          name_attribute: measureModel.dimension.map(dim => dim.level.map(attr => attr.attribute.map(m => m.name))),


        });

        var mergedattribute = [].concat.apply([], this.state.name_attribute);
        var attr = [].concat.apply([], mergedattribute);
        var mergedlevel = [].concat.apply([], this.state.name_level);
        var level = [].concat.apply([], mergedlevel);
        this.setState({ name_attribute: attr, name_level: level });
        console.log("name_attribute", this.state.name_attribute);
        console.log("name_level", this.state.name_level);
        console.log("name_metric", this.state.name_metric);
        console.log("name_dim", this.state.name_dim);




      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })
  }




  fetchspread() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.relaxMetaModelUrl)])
      .then(([relaxModelResponse]) => {
        const metaModel = relaxModelResponse.model_response.relax_model;
        this.setState({
          metaModel, loading: false,
          allSpreads: metaModel.spread,
          spreads: metaModel.spread.filter(spread => spread.name == this.props.match.params.spreadname),
          spread: metaModel.spread.filter(spread => spread.name == this.props.match.params.spreadname)[0],
          name: metaModel.spread.filter(spread => spread.name == this.props.match.params.spreadname)[0].name,
          description: metaModel.spread.filter(spread => spread.name == this.props.match.params.spreadname)[0].description,
          extension_point: metaModel.spread.filter(spread => spread.name == this.props.match.params.spreadname)[0].extension_point,
          target: metaModel.spread.filter(spread => spread.name == this.props.match.params.spreadname)[0].target,
          source_metric: metaModel.spread.filter(spread => spread.name == this.props.match.params.spreadname)[0].source_metric,

        });
        console.log("spreds", this.state.spread.target)



      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }

  editSpread(newSpread) {
    const editspreads = this.state.allSpreads.filter(spread => spread.name !== this.props.match.params.spreadname);
    const edit = [...editspreads, newSpread];
    this.setState({ edit });
    console.log("erfr", this.state.edit);
    const apiUrl = 'http://localhost:55183/frontend/update-relax-model';
    const dataUrl = 'http://localhost:55183/update-relax-model';

    const headers = new Headers();
    const options = {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ ...this.state.metaModel, spread: edit })
    }
    Promise.all([fetch(apiUrl, options), fetch(dataUrl, options)])
      .then(
        (result) => {
          this.setState({
            response: result,
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }
  validate() {
    let targetError = "";
    let source_metricError = "";
    if (!this.state.name_metric.find(v => v === this.state.target)) { targetError = "metric is not valid"; }

    if (!this.state.name_metric.find(v => v === this.state.source_metric)) { source_metricError = "metric is not valid"; }

    if (source_metricError || targetError) {
      this.setState({ source_metricError: source_metricError, targetError: targetError });
      return false;
    }


    return true;
  }


  onSubmit(e) {

    e.preventDefault();
    const newSpread = {
      name: this.refs.name.value,
      description: this.refs.description.value,
      extension_point: this.refs.extension_point.value,
      target: this.refs.target.value,
      source_metric: this.refs.source_metric.value,

    }
    const isValid = this.validate();
    if (isValid) {
      this.editSpread(newSpread);
      this.setState({ targetError: "", source_metricError: "" });
    }




  }
  handleInputChange(e) {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  componentDidMount() {
    this.fetchspread();
    this.fetchMeasure();

  }
  render() {
    const { spreads } = this.state;


    return (<GridContainer><GridItem xs={12} sm={12} md={5}>


      <Card>
        <CardHeader color="success" stats icon>
          <CardIcon color="success">
            <Toc/>
          </CardIcon>
          <p className={useStyles.cardCategory}>{this.props.match.params.spreadname}</p>
        </CardHeader>



        <CardBody>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th >Description</th>
                <th >Extension point</th>
                <th>Target</th>
                <th>Source Metric </th>

              </tr>
            </thead>
            <tbody>
              {spreads.map(spread =>
                <tr key={spread.name}>
                  <td>{spread.description}</td>
                  <td> {spread.extension_point}
                  </td>
                  <td> {spread.target}
                  </td>
                  <td> {spread.source_metric}
                  </td>

                </tr>
              )}
            </tbody>
          </Table>
          <Link className="btn btn-2 btn-sep icon-cart" to="/admin/typography">Back</Link>
        </CardBody>
      </Card>
    </GridItem>
      <GridItem xs={12} sm={12} md={7}>


        <Card>
          <CardHeader color="success">
            <h4 className={useStyles.cardTitleWhite}>Edit {this.props.match.params.spreadname}</h4>

          </CardHeader>
          <CardBody>
            <div>
              <br />

              <form onSubmit={this.onSubmit.bind(this)}>
                <div >
                  <label htmlFor="name">Name :
                  <input disabled type="texta" name="name" ref="name" value={this.state.name} onChange={this.handleInputChange} />
                  </label>
                </div>
                <div>
                  <label htmlFor="description">Description :
                  <input type="texta" name="description" ref="description" value={this.state.description} onChange={this.handleInputChange} />
                  </label>
                </div>
                <div>  <label htmlFor="extension_point">Extension point :
                  <input type="texta" name="extension_point" ref="extension_point" value={this.state.extension_point} onChange={this.handleInputChange} />
                </label>
                </div>
                <div>  <label htmlFor="target">Target :
                  <input type="texta" name="target" ref="target" value={this.state.target} onChange={this.handleInputChange} />

                  {this.state.targetError ?
                    <div className="my-notify-error">{this.state.targetError}</div> : null}
                </label>
                </div>

                <div>
                  <label htmlFor="source_metric">Source Metric :
                    <input type="texta" name="source_metric" ref="source_metric" value={this.state.source_metric} onChange={this.handleInputChange} />
                  </label>
             
                  {this.state.source_metricError?
                    <div className="my-notify-error">{this.state.source_metricError}</div> : null}
                  
                </div>

                <button type="submit" value="Save" className="btn btn-4 btn-sep icon-send" >Save</button>
              </form>
            </div>

          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>);
  }

}



