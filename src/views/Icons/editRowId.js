import React, { Component } from 'react';
import { makeStyles } from "@material-ui/core/styles";

// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import { Link } from "react-router-dom";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import { Table } from 'react-bootstrap';
import axios from 'axios';
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Select from 'react-select';
import Fingerprint from "@material-ui/icons/Fingerprint";
import CardIcon from "components/Card/CardIcon.js";

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import TextField from '@material-ui/core/TextField';
import Warning from "@material-ui/icons/Warning";
import { Container } from '@material-ui/core';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
const defaultMeasuremodel = {
  name: '',
  dimension: [],
  metric: [],
  operator: [],
  type: [],

}
const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};
const useStyles = makeStyles(styles);
const defaultMetamodel = {
  view: [],
  filter: [],
  spread: [],
  row_id_attr: [],

}
const getMetaModelPromise = (url) => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json');
  return fetch(url, {
    headers,
    method: 'POST',
    body: JSON.stringify({ model_request: {} }),

  })
    .then((response) => {
      if (response.ok) {
        return response.json();
      }
      throw new Error(`${response.status}: ${response.statusText}`)
    });
}


export default class EditRowId extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      metaModel: defaultMetamodel,
      measureMetaModelUrl: 'http://localhost:55183/measure',
      relaxMetaModelUrl: 'http://localhost:55183/frontend/globalrelax',
      loading: false,
      relaxMetaModelModal: false,
      rowids: [],
      name: "",
      dimension: "",
      editrowids: [],
      allRowids: [],
      edit: [],
      measureModel: defaultMeasuremodel,
      dimension: [],
      name_dim: [],
      name_level: [],
      name_metric: [],
      name_attribute: [],
      nameError: '',
      dimError: '',
      rowid: []




    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  fetchMeasure() {

    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.measureMetaModelUrl)])
      .then(([measureModelResponse]) => {
        const measureModel = measureModelResponse.model;
        this.setState({
          measureModel, loading: false,
      
          name_dim: measureModel.dimension.map(dim => dim.name),
          name_level: measureModel.dimension.map(dim => dim.level.map(name => name.name)),
   
          name_metric: measureModel.metric.map(metric => metric.name),
          name_attribute: measureModel.dimension.map(dim => dim.level.map(attr => attr.attribute.map(m => m.name))),


        });

        var mergedattribute = [].concat.apply([], this.state.name_attribute);
        var attr = [].concat.apply([], mergedattribute);
        var mergedlevel = [].concat.apply([], this.state.name_level);
        var level = [].concat.apply([], mergedlevel);
        this.setState({ name_attribute: attr, name_level: level });
        console.log("name_attribute", this.state.name_attribute);
        console.log("name_level", this.state.name_level);
        console.log("name_metric", this.state.name_metric);
        console.log("name_dim", this.state.name_dim);




      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })
  }




  fetchrowid() {
    this.setState({ loading: true });
    Promise.all([getMetaModelPromise(this.state.relaxMetaModelUrl),])
      .then(([relaxModelResponse]) => {
        const metaModel = relaxModelResponse.model_response.relax_model;
        this.setState({
          metaModel, loading: false,
          allRowids: metaModel.row_id_attr,
          rowids: metaModel.row_id_attr.filter(rowid => rowid.dimension == this.props.match.params.rowiddim),
          name: metaModel.row_id_attr.filter(rowid => rowid.dimension == this.props.match.params.rowiddim)[0].name,
          dimension: metaModel.row_id_attr.filter(rowid => rowid.dimension == this.props.match.params.rowiddim)[0].dimension,
          rowid: metaModel.row_id_attr.filter(rowid => rowid.dimension == this.props.match.params.rowiddim)[0]

        });
        console.log("rowid", this.state.rowid);


      })

      .catch((error) => {
        this.setState({ loading: false });
        console.error(error)
      })


  }

  editRowId(newRowId) {
    const editrowids = this.state.allRowids.filter(rowid => rowid.dimension == this.props.match.params.rowiddim);
    const edit = [...editrowids, newRowId];
    this.setState({ edit });
    console.log("erfr", this.state.edit);
    const apiUrl = 'http://localhost:55183/frontend/update-relax-model';
    const dataUrl = 'http://localhost:55183/update-relax-model';

    const headers = new Headers();
    const options = {
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
      body: JSON.stringify({ ...this.state.metaModel, row_id_attr: edit })
    }
    Promise.all([fetch(apiUrl, options), fetch(dataUrl, options)])
      .then(
        (result) => {
          this.setState({
            response: result,
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }
  validate() {
    let nameError = "";
    let dimError = "";

    if (!this.state.name_attribute.find(v => v === this.state.name)) { nameError = "name is not valid"; }
    if (!this.state.name_dim.find(v => v === this.state.dimension)) { dimError = "dimension is not valid"; }

    if (nameError || dimError) {
      this.setState({ nameError: nameError, dimError: dimError });
      return false;
    }



    return true ;
  }

  onSubmit(e) {
    e.preventDefault();

    const newRowId = {
      name: this.refs.name.value,
      dimension: this.refs.dimension.value,

    }
    const isValid = this.validate();

    if (isValid) {


      this.editRowId(newRowId);
      this.setState({ nameError: "", dimError: "" });
    }

   

  }
  handleInputChange(e) {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  componentDidMount() {
    this.fetchrowid();
    this.fetchMeasure();

  }
  render() {
    const { rowids } = this.state;


    return (<GridContainer><GridItem xs={12} sm={12} md={4}>
      <Card>
        <CardHeader color="danger" stats icon>
          <CardIcon color="danger">
            <Fingerprint/>
          </CardIcon>
          <p className={useStyles.cardTitleWhite}>{this.props.match.params.rowiddim}</p>
        </CardHeader>



        <CardBody>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th >Dimension</th>
                <th >Name</th>


              </tr>
            </thead>
            <tbody>
              {rowids.map(rowid =>
                <tr key={rowid.dimension}>
                  <td>{rowid.dimension}</td>
                  <td> {rowid.name}</td>
                </tr>
              )}
            </tbody>
          </Table>
          <Link className="btn btn-2 btn-sep icon-cart" to="/admin/icons">Back</Link>
        </CardBody>
      </Card>
    </GridItem>
      <GridItem xs={12} sm={12} md={8}>
        <Card>
          <CardHeader color="success">
            <h4 className={useStyles.cardTitleWhite}>Edit {this.props.match.params.rowiddim}</h4>

          </CardHeader>
          <CardBody>
            <div>
              <br />

              <form onSubmit={this.onSubmit.bind(this)}>
                <div>
                <label htmlFor="name">Name :
                    <input type="texta" name="name" ref="name" value={this.state.name} onChange={this.handleInputChange} />
                
                    {this.state.nameError?
                    <div className="my-notify-error">{this.state.nameError}</div> : null}
             
                  </label>
                </div>
                <div>
                <label htmlFor="dimension">Dimension :
                  <input type="texta" name="dimension" ref="dimension" value={this.state.dimension} onChange={this.handleInputChange} />
                  {this.state.dimError?
                    <div className="my-notify-error">{this.state.dimError}</div> : null}
               
                </label>
                </div>

                <button type="submit" value="Save" className="btn btn-4 btn-sep icon-send" >Save</button>
               
              </form>
            </div>

          </CardBody>
        </Card>
      </GridItem> </GridContainer>);
  }

}

