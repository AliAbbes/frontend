/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

// core components
import Admin from "layouts/Admin.js";
import RTL from "layouts/RTL.js";
import EditView from "views/UserProfile/editView.js";
import EditFilter from "views/TableList/editFilter.js";
import EditSpread from "views/Typography/editSpread.js";
import EditRowId from "views/Icons/editRowId.js";
import Login from "views/RTLPage/RTLPage";
import "assets/css/material-dashboard-react.css?v=1.8.0";
import './index.less';

const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>

      <Route path="/editRowId/:rowiddim" component={EditRowId}/>
      <Route path="/editSpread/:spreadname" component={EditSpread}/>
      <Route path="/editView/:viewname" component={EditView}/>
      <Route path="/editFilter/:filtername" component={EditFilter}/>
      <Route path="/admin" component={Admin} />
      <Route path="/rtl" component={RTL} />
      <Route path="/login" component={Login}/>
      <Redirect from="/" to="/admin/dashboard" />
    </Switch>
  </Router>,
  document.getElementById("root")
);
